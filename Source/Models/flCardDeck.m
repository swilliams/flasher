//
//  flCardDeck.m
//  Flasher
//
//  Created by Scott Williams on 10/17/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import "flCardDeck.h"
#import "flQuestionAnswer.h"
#import "NSMutableArray+QuestionCounting.h"
#import <stdlib.h>

@implementation flCardDeck

@synthesize deckName, createdAt, lastUpdated, cards, serverId, currentIndex, activeCards;

#pragma mark - Setup
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.deckName = [aDecoder decodeObjectForKey:@"deckName"];
        self.createdAt = [aDecoder decodeDoubleForKey:@"createdAt"];
        self.lastUpdated = [aDecoder decodeDoubleForKey:@"lastUpdated"];
        self.serverId = [aDecoder decodeInt32ForKey:@"serverId"];
        self.cards = [aDecoder decodeObjectForKey:@"cards"];
        if (!self.cards) {
            self.cards = [[NSMutableArray alloc] init];
        }
        [self setupDeck];
    }
    return self;
}

- (id)init {
    self = [super init];
    if (self) {
        self.cards = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:deckName forKey:@"deckName"];
    [aCoder encodeDouble:createdAt forKey:@"createdAt"];
    [aCoder encodeDouble:lastUpdated forKey:@"lastUpdated"];
    [aCoder encodeInt32:serverId forKey:@"serverId"];
    [aCoder encodeObject:cards forKey:@"cards"];
}

- (void)setupDeck {
    currentIndex = -1;
    results = [NSMutableArray arrayWithCapacity:self.activeCards.count];
}

#pragma mark - Getters
- (int)numberOfRightAnswers {
    return [[self.activeCards allRightQuestions] count];
}

- (int)numberOfWrongAnswers {
    return [[self.activeCards allWrongQuestions] count];
}

- (int)numberOfSkippedAnswers {
    return [[self.activeCards allSkippedQuestions] count];
}

- (NSMutableArray *)activeCards {
    if (!activeCards) {
        activeCards = [NSMutableArray arrayWithArray:cards];
    }
    return activeCards;
}

#pragma mark - Actions
- (void)addNewQuestion:(flQuestionAnswer *)question {
    if (![cards containsObject:question]) {
        [cards addObject:question];
        [self.activeCards addObject:question];
    }
}

- (void)removeQuestion:(flQuestionAnswer *)question {
    if ([cards containsObject:question]) {
        [cards removeObject:question];
        [self.activeCards removeObject:question];
    }
}

- (flQuestionAnswer *)nextQuestion {
    if ([self.activeCards count] == 0) {
        return nil;
    }
    if (currentIndex >= [self.activeCards count] - 1) {
        currentIndex = 0;
    } else {
        currentIndex += 1;
    }
    flQuestionAnswer *next = [self.activeCards objectAtIndex:currentIndex];
    return next;
}

- (flQuestionAnswer *)peakNext {
    int count = [self.activeCards count];
    if (count == 0 || currentIndex+1 >= count) {
        return nil;
    }
    return [self.activeCards objectAtIndex:currentIndex+1];
}

- (flQuestionAnswer *)prevQuestion {
    if ([self.activeCards count] == 0) {
        return nil;
    }
    if (currentIndex <= 0) {
        currentIndex = [self.activeCards count] - 1;
    } else {
        currentIndex -= 1;
    }
    flQuestionAnswer *prev = [self.activeCards objectAtIndex:currentIndex];
    return prev;
}

- (flQuestionAnswer *)peakPrev {
    if (currentIndex <= 0) {
        return nil;
    }
    return [self.activeCards objectAtIndex:currentIndex-1];
}

- (void)randomizeQuestions {
    int count = [self.activeCards count];
    for (int i = 0; i < count; ++i) {
        int nElements = count - i;
        int n = (arc4random_uniform(nElements));
        [self.activeCards exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
    currentIndex = -1;
}

- (BOOL)isFromServer {
    return self.serverId != 0;
}

- (void)addRightQuestion:(flQuestionAnswer *)question {
    question.answered = YES;
    [results addRight:question];
}

- (void)addWrongQuestion:(flQuestionAnswer *)question {
    question.answered = YES;
    [results addWrong:question];
}

- (void)addSkippedQuestion:(flQuestionAnswer *)question {
    question.answered = NO;
    [results addSkipped:question];
}

- (void)startOverWith:(NSArray *)questions {
    activeCards = [NSMutableArray arrayWithArray:questions];
    [activeCards resetAnswers];
    [self setupDeck];
}

- (void)startOverWithWrongQuestions {
    [self startOverWith:[self.activeCards allWrongQuestions]];
}

- (void)startWithAllQuestions {
    [self startOverWith:cards];
}

- (void)printResults {
    int right = [results allRightQuestions].count;
    int wrong = [[results allWrongQuestions] count];
    int skipped = [[results allSkippedQuestions] count];
    NSLog(@"right: %d\nwrong: %d\nskipped: %d", right, wrong, skipped);
}

#pragma mark -
#pragma mark JSON Stuff
- (void)readFromJSONDictionary:(NSDictionary *)d {
    // set properties, then create questions and recursively call read on them
    self.deckName = [d objectForKey:@"deckname"];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    NSDate *tmpDate = [formatter dateFromString:[d objectForKey:@"created_at"]];
    self.createdAt = [tmpDate timeIntervalSince1970];
    
    NSDate *tmpUpdated = [formatter dateFromString:[d objectForKey:@"updated_at"]];
    self.lastUpdated = [tmpUpdated timeIntervalSince1970];

    self.serverId = (NSInteger)[d objectForKey:@"id"];
    
    NSArray *questionsArray = [d objectForKey:@"question_answers"];
    self.cards = [[NSMutableArray alloc] initWithCapacity:questionsArray.count];
    for (NSDictionary *questionDict in questionsArray) {
        flQuestionAnswer *q = [[flQuestionAnswer alloc] init];
        [q readFromJSONDictionary:questionDict];
        [self.cards addObject:q];
    }
}

- (NSString *)toHttpString {
    NSMutableString *postData = [NSMutableString stringWithFormat:@"deckname=%@&enduser_id=%d", [deckName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], 1];
    for (int i = 0; i < cards.count; i++) {
        flQuestionAnswer *q = [cards objectAtIndex:i];
        NSString *prefix = [NSString stringWithFormat:@"question_answers[%d]", i];
        [postData appendFormat:@"&%@", [q toHttpString:prefix]];
    }
    return postData;
}

@end
