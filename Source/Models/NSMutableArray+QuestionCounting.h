//
//  NSMutableArray+QuestionCounting.h
//  Flasher
//
//  Created by Scott Williams on 1/1/13.
//  Copyright (c) 2013 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>

@class flQuestionAnswer;
@interface NSMutableArray (QuestionCounting)

- (void)addRight:(flQuestionAnswer *)rightQuestion;
- (void)addWrong:(flQuestionAnswer *)wrongQuestion;
- (void)addSkipped:(flQuestionAnswer *)skippedQuestion;
- (NSArray *)allRightQuestions;
- (NSArray *)allWrongQuestions;
- (NSArray *)allSkippedQuestions;
- (void)resetAnswers;


@end
