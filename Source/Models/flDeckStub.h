//
//  flDeckStub.h
//  Flasher
//
//  Created by Scott Williams on 11/4/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONSerializeable.h"

@interface flDeckStub : NSObject <JSONSerializeable>

@property (nonatomic) int deckId;
@property (nonatomic, strong) NSString *deckName;
@property (nonatomic, strong) NSString *ownerName;
@property (nonatomic, strong) NSDate *createdAt;
@property (nonatomic) int questionCount;


@end
