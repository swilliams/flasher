//
//  SelectedDeckLoader.m
//  Flasher
//
//  Created by Scott Williams on 11/5/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import "SelectedDeckLoader.h"
#import "flCardDeck.h"
#import "flConstants.h"

@implementation SelectedDeckLoader

- (void)connectionDidFinishLoading:(NSURLConnection *)conn {
    NSMutableArray *decks = [[NSMutableArray alloc] init];
    NSArray *results = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
    for (NSDictionary *d in results) {
        flCardDeck *deck = [[flCardDeck alloc] init];
        [deck readFromJSONDictionary:d];
        [decks addObject:deck];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:flDecksLoaded object:self userInfo:[NSDictionary dictionaryWithObject:decks forKey:@"decks"]];
}

@end
