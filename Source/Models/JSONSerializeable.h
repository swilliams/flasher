//
//  JSONSerializeable.h
//  Flasher
//
//  Created by Scott Williams on 11/4/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol JSONSerializeable <NSObject>

- (void)readFromJSONDictionary:(NSDictionary *)d;

@end
