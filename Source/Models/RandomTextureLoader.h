//
//  RandomTextureLoader.h
//  Flasher
//
//  Created by Scott Williams on 12/7/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RandomTextureLoader : NSObject {
    NSMutableArray *imageNames;
    int currentIndex;
}

- (void)createImageNames;
- (NSString *)getNext;

@end
