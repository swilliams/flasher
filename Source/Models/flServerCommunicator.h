//
//  flServerCommunicator.h
//  Flasher
//
//  Created by Scott Williams on 11/3/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONSerializeable.h"

@class flCardDeck;

@interface flServerCommunicator : NSObject {
    NSURLConnection *connection;
}

+ (flServerCommunicator *)sharedCommunicator;

@property (nonatomic, strong) id <JSONSerializeable> jsonRootObject;
@property BOOL hasConnection;

- (void)fetchRecentDecks;
- (void)fetchDecks:(NSArray *)deckIds;
- (void)destroySingleDeck:(int)deckId;
- (void)createDeck:(flCardDeck *)deck;
- (void)updateDeck:(flCardDeck *)deck;
- (void)checkForUploadAuthorization;

@end
