//
//  AuthManager.m
//  Flasher
//
//  Created by Scott Williams on 12/1/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import "AuthManager.h"
#import "flConstants.h"

@implementation AuthManager

@synthesize authorized;

+ (AuthManager *)sharedManager {
    static AuthManager *manager = nil;
    if (!manager) {
        manager = [[super allocWithZone:nil] init];
    }
    return manager;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [self sharedManager];
}

- (id)init {
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(authorizedFinished:) name:flAuthorizedForUpload object:nil];
    }
    return self;
}

- (void)authorizedFinished:(NSNotification *)n {
    authorized = YES;
}

@end
