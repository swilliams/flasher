//
//  flDeckStore.m
//  Flasher
//
//  Created by Scott Williams on 10/17/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import "flDeckStore.h"
#import "flCardDeck.h"
#import "flQuestionAnswer.h"

@implementation flDeckStore

+ (flDeckStore *)sharedStore {
    static flDeckStore *store = nil;
    if (!store) {
        store = [[super allocWithZone:nil] init];
    }
    return store;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [self sharedStore];
}

- (id)init {
    self = [super init];
    if (self) {
        [self loadAllDecks];
    }
    return self;
}

- (NSArray *)allDecks {
    return allDecks;
}

- (NSString *)deckArchivePath {
    NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [documentDirectories objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:@"carddecks.archive"];
}

- (void)loadAllDecks {
    NSString *path = [self deckArchivePath];
    allDecks = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    if (!allDecks) {
        [self loadDummyData];
    }
}

- (flCardDeck *)createDeckWithName:(NSString *)deckName {
    flCardDeck *newDeck = [[flCardDeck alloc] init];
    newDeck.deckName = deckName;
    [allDecks addObject:newDeck];
    return newDeck;
}

- (void)addDecks:(NSArray *)decks {
    [allDecks addObjectsFromArray:decks];
}

- (BOOL)deleteDeck:(flCardDeck *)deck {
    [allDecks removeObject:deck];
    return [self saveChanges];
}

- (BOOL)saveChanges {
    NSString *path = [self deckArchivePath];
    NSLog(@"saving to: %@", path);
    return [NSKeyedArchiver archiveRootObject:allDecks toFile:path];
}

- (void)loadDummyData {
    NSLog(@"loading dummy data");
    allDecks = [[NSMutableArray alloc] init];
    flCardDeck *deck1 = [[flCardDeck alloc] init];
    deck1.deckName = @"Starter Questions";
        
    flQuestionAnswer *q1 = [[flQuestionAnswer alloc] init];
    q1.questionText = @"Who is buried in Grants' Tomb?";
    q1.answerText = @"Grant";
    flQuestionAnswer *q2 = [[flQuestionAnswer alloc] initWithQuestion:@"What color is the White House" andAnswer:@"White"];
    flQuestionAnswer *q3 = [[flQuestionAnswer alloc] initWithQuestion:@"It’s your birthday. Someone gives you a calfskin wallet. How do you react?" andAnswer:@"I wouldn't accept it."];
    [deck1.cards addObjectsFromArray:@[q1, q2, q3]];
    
    
    [allDecks addObject:deck1];
}

@end
