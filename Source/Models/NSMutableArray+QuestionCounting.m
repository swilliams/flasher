//
//  NSMutableArray+QuestionCounting.m
//  Flasher
//
//  Created by Scott Williams on 1/1/13.
//  Copyright (c) 2013 Scott Williams. All rights reserved.
//

#import "NSMutableArray+QuestionCounting.h"
#import "flQuestionAnswer.h"

@implementation NSMutableArray (QuestionCounting)

- (void)addRight:(flQuestionAnswer *)rightQuestion {
    [self removeExistingAnswer:rightQuestion];

    rightQuestion.answeredCorrectly = YES;
    [self addObject:rightQuestion];
}

- (void)addWrong:(flQuestionAnswer *)wrongQuestion {
    [self removeExistingAnswer:wrongQuestion];
    
    wrongQuestion.answeredCorrectly = NO;
    [self addObject:wrongQuestion];
}

- (void)addSkipped:(flQuestionAnswer *)skippedQuestion {
    [self removeExistingAnswer:skippedQuestion];
    
    skippedQuestion.skipped = YES;
    [self addObject:skippedQuestion];
}

- (void)removeExistingAnswer:(flQuestionAnswer *)question {
    NSUInteger index = [self indexOfObjectPassingTest:^BOOL (id obj, NSUInteger idx, BOOL *stop) {
        flQuestionAnswer *q = (flQuestionAnswer *)obj;
        BOOL qMatched = [q.questionText isEqualToString:question.questionText];
        BOOL aMatched = [q.answerText isEqualToString:question.answerText];
        return qMatched && aMatched;
    }];
    if (index != NSNotFound) {
        [self removeObjectAtIndex:index];
    }
}


- (NSArray *)allRightQuestions {
    return [self filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id obj, NSDictionary *bindingOptions) {
        flQuestionAnswer *q = (flQuestionAnswer *)obj;
        return q.answered && q.answeredCorrectly;
    }]];
}

- (NSArray *)allWrongQuestions {
    return [self filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id obj, NSDictionary *bindingOptions) {
        flQuestionAnswer *q = (flQuestionAnswer *)obj;
        return q.answered && !q.answeredCorrectly;
    }]];
}

- (NSArray *)allSkippedQuestions {
    return [self filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id obj, NSDictionary *bindingOptions) {
        flQuestionAnswer *q = (flQuestionAnswer *)obj;
        return !q.answered && q.skipped;
    }]];
}

- (void)resetAnswers {
    [self enumerateObjectsUsingBlock:^(id question, NSUInteger idx, BOOL *stop) {
        flQuestionAnswer *q = (flQuestionAnswer *)question;
        q.skipped = NO;
        q.answered = NO;
    }];
}


@end
