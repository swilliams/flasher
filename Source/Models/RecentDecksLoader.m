//
//  RecentDecksLoader.m
//  Flasher
//
//  Created by Scott Williams on 11/4/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import "RecentDecksLoader.h"
#import "flDeckStub.h"
#import "flConstants.h"

@implementation RecentDecksLoader

- (void)connectionDidFinishLoading:(NSURLConnection *)conn {
    NSMutableArray *decks = [[NSMutableArray alloc] init];
    NSArray *results = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
    for (NSDictionary *d in results) {
        flDeckStub *stub = [[flDeckStub alloc] init];
        [stub readFromJSONDictionary:d];
        [decks addObject:stub];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:flRecentDecksLoaded object:self userInfo:[NSDictionary dictionaryWithObject:decks forKey:@"decks"]];
}

@end
