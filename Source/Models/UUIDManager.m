//
//  UUIDManager.m
//  Flasher
//
//  Created by Scott Williams on 12/1/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import "UUIDManager.h"

@implementation UUIDManager

+ (UUIDManager *)sharedManager {
    static UUIDManager *manager = nil;
    if (!manager) {
        manager = [[super allocWithZone:nil] init];
    }
    return manager;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [self sharedManager];
}

- (id)init {
    self = [super init];
    if (self) {
        [self loadFromDisk];
    }
    return self;
}

- (NSString *)createUUID {
    NSString *uuid = nil;
    CFUUIDRef u = CFUUIDCreate(NULL);
    if (u) {
        uuid = (__bridge NSString *)CFUUIDCreateString(NULL, u);
        CFRelease(u);
    }
    NSLog(@"created uuid: %@", uuid);
    return uuid;
}

- (NSString *)archivePath {
    NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [documentDirectories objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:@"uuid.archive"];
}

- (void)loadFromDisk {
    NSString *path = [self archivePath];
    deviceId = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    if (!deviceId) {
        deviceId = [self createUUID];
        [self saveChanges];
    }
}

- (NSString *)deviceId {
    return deviceId;
}

- (BOOL)saveChanges {
    return [NSKeyedArchiver archiveRootObject:deviceId toFile:[self archivePath]];
}

@end
