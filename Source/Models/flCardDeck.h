//
//  flCardDeck.h
//  Flasher
//
//  Created by Scott Williams on 10/17/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONSerializeable.h"

@class flQuestionAnswer;

@interface flCardDeck : NSObject <NSCoding, JSONSerializeable> {
    int currentIndex;
    NSMutableArray *results;
    NSMutableArray *activeCards;
}

@property (nonatomic, retain) NSString *deckName;
@property (nonatomic) NSTimeInterval createdAt;
@property (nonatomic) NSTimeInterval lastUpdated;
@property (nonatomic, retain) NSMutableArray *cards;
@property (readonly, nonatomic, retain) NSMutableArray *activeCards;
@property int serverId;
@property int currentIndex;

- (flQuestionAnswer *)nextQuestion;
- (flQuestionAnswer *)prevQuestion;
- (flQuestionAnswer *)peakNext;
- (flQuestionAnswer *)peakPrev;
- (int)numberOfWrongAnswers;
- (int)numberOfRightAnswers;
- (int)numberOfSkippedAnswers;

- (void)randomizeQuestions;
- (NSString *)toHttpString;
- (BOOL)isFromServer;

- (void)addNewQuestion:(flQuestionAnswer *)question;
- (void)removeQuestion:(flQuestionAnswer *)question;

- (void)addRightQuestion:(flQuestionAnswer *)question;
- (void)addWrongQuestion:(flQuestionAnswer *)question;
- (void)addSkippedQuestion:(flQuestionAnswer *)question;
- (void)startOverWithWrongQuestions;
- (void)startWithAllQuestions;
- (void)printResults;

@end
