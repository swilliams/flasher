//
//  DeckUploader.m
//  Flasher
//
//  Created by Scott Williams on 11/28/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import "DeckUploader.h"
#import "flConstants.h"

@implementation DeckUploader

- (id)init {
    return [self initForHttpVerb:@"POST"];
}

- (id)initForHttpVerb:(NSString *)verb {
    httpVerb = verb;
    return [super init];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)conn {
    if ([httpVerb isEqualToString:@"POST"]) {
        NSString *result = [[NSString alloc] initWithData:jsonData encoding:NSASCIIStringEncoding];
        NSInteger createdDeckId = [result integerValue];
        [[NSNotificationCenter defaultCenter] postNotificationName:flDeckCreated object:self userInfo:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:createdDeckId] forKey:@"createdDeckId"]];
    } else if([httpVerb isEqualToString:@"PUT"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:flDeckUpdated object:self];
    }

}
@end
