//
//  BaseJsonLoader.m
//  Flasher
//
//  Created by Scott Williams on 11/4/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import "BaseJsonLoader.h"
#import "flConstants.h"

@implementation BaseJsonLoader

- (id)init {
    self = [super init];
    if (self) {
        jsonData = [[NSMutableData alloc] init];
    }
    return self;
}

- (void)connection:(NSURLConnection *)conn didReceiveData:(NSData *)data {
    [jsonData appendData:data];
}

- (void)connection:(NSURLConnection *)conn didFailWithError:(NSError *)error {
    conn = nil;
    jsonData = nil;
    NSString *errorString = [NSString stringWithFormat:@"Server Error: %@", [error localizedDescription]];
    [[NSNotificationCenter defaultCenter] postNotificationName:FLRemoteError object:self userInfo:@{@"errorString": errorString}];
}


@end
