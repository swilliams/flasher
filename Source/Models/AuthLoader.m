//
//  AuthLoader.m
//  Flasher
//
//  Created by Scott Williams on 12/1/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import "AuthLoader.h"
#import "flConstants.h"

@implementation AuthLoader
- (void)connectionDidFinishLoading:(NSURLConnection *)conn {
    NSString *result = [[NSString alloc] initWithData:jsonData encoding:NSASCIIStringEncoding];
    if ([result boolValue]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:flAuthorizedForUpload object:self];
    }
}
@end
