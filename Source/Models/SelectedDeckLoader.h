//
//  SelectedDeckLoader.h
//  Flasher
//
//  Created by Scott Williams on 11/5/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import "BaseJsonLoader.h"

@interface SelectedDeckLoader : BaseJsonLoader

@end
