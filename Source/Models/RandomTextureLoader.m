//
//  RandomTextureLoader.m
//  Flasher
//
//  Created by Scott Williams on 12/7/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import "RandomTextureLoader.h"

@implementation RandomTextureLoader

- (id)init {
    self = [super init];
    if (self) {
        [self createImageNames];
        [self randomizeImageNames];
    }
    return self;
}

- (void)createImageNames {
    imageNames = [NSMutableArray arrayWithObjects:
                  @"hexabump",
                  @"always_grey",
                  @"brushed_alu_dark",
                  @"classy_fabric",
                  @"dark_stripes",
                  @"random_grey_variations", nil];
}

- (void)randomizeImageNames {
    int count = [imageNames count];
    for (int i = 0; i < count; ++i) {
        int nElements = count - 1;
        int n = (arc4random_uniform(nElements));
        [imageNames exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
    currentIndex = 0;
}

- (NSString *)getNext {
    if (currentIndex >= imageNames.count) {
        currentIndex = 0;
    }
    NSString *next = [imageNames objectAtIndex:currentIndex];
    currentIndex += 1;
    return next;
}


@end
