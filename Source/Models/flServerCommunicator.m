//
//  flServerCommunicator.m
//  Flasher
//
//  Created by Scott Williams on 11/3/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import "flServerCommunicator.h"
#import "RecentDecksLoader.h"
#import "SelectedDeckLoader.h"
#import "DeckUploader.h"
#import "flCardDeck.h"
#import "UUIDManager.h"
#import "AuthLoader.h"
#import "Reachability.h"

static NSString *remoteServer = @"http://flashcardsapp.herokuapp.com";
//static NSString *localServer = @"http://10.0.1.53:3000";
static NSString *localServer = @"http://192.168.1.107:3000";

@implementation flServerCommunicator
@synthesize jsonRootObject, hasConnection;

#pragma mark -
#pragma mark Setup
+ (flServerCommunicator *)sharedCommunicator {
    static flServerCommunicator *communicator = nil;
    if (!communicator) {
        communicator = [[super allocWithZone:nil] init];
    }
    return communicator;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [self sharedCommunicator];
}

- (id)init {
    self = [super init];
    if (self) {
//        [self setupReachability];
    }
    return self;
}

- (void)setupReachability {
    NSLog(@"checking");
    Reachability *reacher = [Reachability reachabilityWithHostname:@"www.google.com"];
    reacher.reachableBlock = ^(Reachability *reach) {
        NSLog(@"yes");
        self.hasConnection = YES;
    };
    reacher.unreachableBlock = ^(Reachability *reach) {
        NSLog(@"no");
        self.hasConnection = NO;
    };
    [reacher startNotifier];
}

- (NSString *)urlRoot {
    NSString *device = [[UIDevice currentDevice] model];
    return [device isEqualToString:@"iPad Simulator"] ? localServer : remoteServer;
}

#pragma mark -
#pragma mark URL Builder
- (NSString *)deckUrl:(NSString *)suffix {
    return [NSString stringWithFormat:@"%@/decks/%@", self.urlRoot, suffix];
}

- (NSString *)authUrl:(NSString *)uuid {
    return [NSString stringWithFormat:@"%@/auth?udid=%@", self.urlRoot, uuid];
}

#pragma mark -
#pragma mark API Actions
- (void)fetchRecentDecks {
    RecentDecksLoader *loader = [[RecentDecksLoader alloc] init];
    NSURL *url = [NSURL URLWithString:[self deckUrl:@""]];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    connection = [[NSURLConnection alloc] initWithRequest:req delegate:loader startImmediately:YES];
}

- (void)fetchDecks:(NSArray *)deckIds {
    SelectedDeckLoader *loader = [[SelectedDeckLoader alloc] init];
    NSString *s = [self deckUrl:@"select_multiple"];
    NSURL *url = [NSURL URLWithString:s];
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:90];
    req.HTTPMethod = @"POST";
    [req setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    NSMutableString *postData = [NSMutableString stringWithString:@""];
    for (NSNumber *deckId in deckIds) {
        [postData appendFormat:@"multiple_ids[]=%d&", deckId.intValue];
    }
    req.HTTPBody = [postData dataUsingEncoding:NSUTF8StringEncoding];
    connection = [[NSURLConnection alloc] initWithRequest:req delegate:loader startImmediately:YES];
}

- (void)destroySingleDeck:(int)deckId {
    NSLog(@"destroy");
}

- (void)createDeck:(flCardDeck *)deck {
    DeckUploader *uploader = [[DeckUploader alloc] initForHttpVerb:@"POST"];
    NSString *suffix = [NSString stringWithFormat:@"?udid=%@", [[UUIDManager sharedManager] deviceId]];
    NSURL *url = [NSURL URLWithString:[self deckUrl:suffix]];
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:90];
    req.HTTPMethod = @"POST";
    [req setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    req.HTTPBody = [[deck toHttpString] dataUsingEncoding:NSUTF8StringEncoding];
    connection = [[NSURLConnection alloc] initWithRequest:req delegate:uploader startImmediately:YES];
}

- (void)updateDeck:(flCardDeck *)deck {
    DeckUploader *uploader = [[DeckUploader alloc] initForHttpVerb:@"PUT"];
    NSString *suffix = [NSString stringWithFormat:@"%d?udid=%@", deck.serverId, [[UUIDManager sharedManager] deviceId]];
    NSURL *url = [NSURL URLWithString:[self deckUrl:suffix]];
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:90];
    req.HTTPMethod = @"PUT";
    [req setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    req.HTTPBody = [[deck toHttpString] dataUsingEncoding:NSUTF8StringEncoding];
    connection = [[NSURLConnection alloc] initWithRequest:req delegate:uploader startImmediately:YES];
}

- (void)checkForUploadAuthorization {
    AuthLoader *authLoader = [[AuthLoader alloc] init];
    NSString *uuid = [[UUIDManager sharedManager] deviceId];
    NSLog(@"authing: %@", uuid);
    NSURL *url = [NSURL URLWithString:[self authUrl:uuid]];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    connection = [[NSURLConnection alloc] initWithRequest:req delegate:authLoader startImmediately:YES];
}

@end
