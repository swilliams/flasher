//
//  UUIDManager.h
//  Flasher
//
//  Created by Scott Williams on 12/1/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UUIDManager : NSObject {
    NSString *deviceId;
}

+ (UUIDManager *)sharedManager;

- (NSString *)deviceId;

@end
