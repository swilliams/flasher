//
//  AuthManager.h
//  Flasher
//
//  Created by Scott Williams on 12/1/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AuthManager : NSObject

@property BOOL authorized;

+ (AuthManager *)sharedManager;


@end
