//
//  flDeckStub.m
//  Flasher
//
//  Created by Scott Williams on 11/4/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import "flDeckStub.h"

@implementation flDeckStub

@synthesize deckName, ownerName, createdAt, questionCount, deckId;

- (void)readFromJSONDictionary:(NSDictionary *)d {
    self.deckName = [d objectForKey:@"deckname"];
    self.ownerName = [d objectForKey:@"ownername"];
    NSNumber *nsDeckId = [d objectForKey:@"id"];
    self.deckId = [nsDeckId integerValue];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    self.createdAt = [formatter dateFromString:[d objectForKey:@"created_at"]];
    NSNumber *nsQuestionCount = [d objectForKey:@"question_count"];
    self.questionCount = [nsQuestionCount integerValue];
}

@end
