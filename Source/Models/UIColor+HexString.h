//
//  UIColor+HexString.h
//  Flasher
//
//  Created by Scott Williams on 2/6/13.
//  Copyright (c) 2013 Scott Williams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexString)

- (UIColor *)lighterColorForColor:(UIColor *)c;

- (UIColor *)darkerColor:(float)darker;

+ (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length;
+ (UIColor *) colorWithHexString: (NSString *) hexString;

@end
