//
//  flDeckStore.h
//  Flasher
//
//  Created by Scott Williams on 10/17/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>

@class flCardDeck;

@interface flDeckStore : NSObject {
    NSMutableArray *allDecks;
}

+ (flDeckStore *) sharedStore;

- (NSString *)deckArchivePath;
- (NSArray *)allDecks;
- (void)loadAllDecks;
- (BOOL)deleteDeck:(flCardDeck *)deck;
- (BOOL)saveChanges;
- (flCardDeck *)createDeckWithName:(NSString *)deckName;
- (void)addDecks:(NSArray *)decks;

@end
