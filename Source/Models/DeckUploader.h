//
//  DeckUploader.h
//  Flasher
//
//  Created by Scott Williams on 11/28/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import "BaseJsonLoader.h"

@interface DeckUploader : BaseJsonLoader {
    NSString *httpVerb;
}

- (id)initForHttpVerb:(NSString *)verb;

@end
