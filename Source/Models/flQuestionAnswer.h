//
//  flQuestionAnswer.h
//  Flasher
//
//  Created by Scott Williams on 10/17/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSONSerializeable.h"

@interface flQuestionAnswer : NSObject <NSCoding, JSONSerializeable> {
    BOOL answeredCorrectly;
}

- (id)initWithQuestion:(NSString *)question andAnswer:(NSString *)answer;
- (void)readFromJSONDictionary:(NSDictionary *)d;
- (NSString *)toHttpString:(NSString *)prefix;

@property (nonatomic, retain) NSString *questionText;
@property (nonatomic, retain) NSString *answerText;
@property BOOL skipped;
@property BOOL answered;
@property BOOL answeredCorrectly;

@end
