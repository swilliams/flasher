//
//  flQuestionAnswer.m
//  Flasher
//
//  Created by Scott Williams on 10/17/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import "flQuestionAnswer.h"

@implementation flQuestionAnswer

@synthesize questionText, answerText, skipped, answered;

- (id)initWithQuestion:(NSString *)question andAnswer:(NSString *)answer {
    self = [super init];
    if (self) {
        self.questionText = question;
        self.answerText = answer;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.questionText = [aDecoder decodeObjectForKey:@"questionText"];
        self.answerText = [aDecoder decodeObjectForKey:@"answerText"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:questionText forKey:@"questionText"];
    [aCoder encodeObject:answerText forKey:@"answerText"];
}

- (void)readFromJSONDictionary:(NSDictionary *)d {
    self.questionText = [d objectForKey:@"question"];
    self.answerText = [d objectForKey:@"answer"];
}

- (NSString *)toHttpString:(NSString *)prefix {
    return [NSString stringWithFormat:@"%@[question]=%@&%@[answer]=%@", prefix, [questionText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], prefix, [answerText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ - %@", self.questionText, self.answerText];
}

- (void)setAnsweredCorrectly:(BOOL)val {
    self.answered = YES;
    answeredCorrectly = val;
}

- (BOOL)answeredCorrectly {
    return answeredCorrectly;
}

@end
