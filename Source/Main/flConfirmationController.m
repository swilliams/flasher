//
//  flConfirmationController.m
//  Flasher
//
//  Created by Scott Williams on 10/31/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import "flConfirmationController.h"

@implementation flConfirmationController

@synthesize didConfirm, delegate;

- (IBAction)confirmTapped:(id)sender {
    didConfirm = true;

    if ([self.delegate respondsToSelector:@selector(viewControllerFinishedOperations:)]) {
        [self.delegate viewControllerFinishedOperations:self];
    }

}

@end
