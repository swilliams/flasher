//
//  flDownloadController.m
//  Flasher
//
//  Created by Scott Williams on 11/3/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import "flDownloadController.h"
#import "flDownloadTableCell.h"
#import "flServerCommunicator.h"
#import "flDeckStub.h"
#import "flConstants.h"
#import "MBProgressHUD.h"

NSString *kflDownloadTableCell = @"kflDownloadTableCell";

@implementation flDownloadController

#pragma mark -
#pragma mark Setup
- (id)init {
    self = [super init];
    if (self) {
        UINavigationItem *n = [self navigationItem];
        n.title = @"Download Decks";
        
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneTapped:)];
        n.rightBarButtonItem = doneButton;
        selectedStubs = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UINib *nib = [UINib nibWithNibName:@"flDownloadTableCell" bundle:nil];
    [tableView registerNib:nib forCellReuseIdentifier:kflDownloadTableCell];

    [self setupNotificationObservers];
    [MBProgressHUD showHUDAddedTo:tableView animated:YES];
    [[flServerCommunicator sharedCommunicator] fetchRecentDecks];
}

- (void)setupNotificationObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataLoaded:) name:flRecentDecksLoaded object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(decksDownloaded:) name:flDecksLoaded object:nil];
}

#pragma mark -
#pragma mark TableView Delegates
- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    flDownloadTableCell *cell = [tv dequeueReusableCellWithIdentifier:kflDownloadTableCell];
    flDeckStub *stub = [deckStubs objectAtIndex:[indexPath row]];

    cell.decknameLabel.text = stub.deckName;
    cell.ownernameLabel.text = stub.ownerName;
    cell.questionCountLabel.text = [NSString stringWithFormat:@"%d", stub.questionCount];
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateStyle = NSDateFormatterMediumStyle;
        dateFormatter.timeStyle = NSDateFormatterNoStyle;
    }
    cell.createdDateLabel.text = [dateFormatter stringFromDate:stub.createdAt];
    cell.tag = stub.deckId;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tv numberOfRowsInSection:(NSInteger)section {
    if (deckStubs) {
        return [deckStubs count];
    }
    return 0;
}

- (IBAction)doneTapped:(id)sender {
    if (selectedStubs.count > 0) {
        [self downloadSelectedDecks];
    } else {
        [[self parentViewController] dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    flDownloadTableCell *selectedCell = (flDownloadTableCell *)[tv cellForRowAtIndexPath:indexPath];
    [selectedCell toggleCheck];
    if (selectedCell.checked) {
        [selectedStubs addObject:[deckStubs objectAtIndex:[indexPath row]]];
    } else {
        [selectedStubs removeObject:[deckStubs objectAtIndex:[indexPath row]]];
    }
}

#pragma mark -
#pragma mark Data delegates
- (void)dataLoaded:(NSNotification *)note {
    deckStubs = [[note userInfo] objectForKey:@"decks"];
    [tableView reloadData];
    [MBProgressHUD hideHUDForView:tableView animated:YES];
}

- (void)decksDownloaded:(NSNotification *)note {
    
    [MBProgressHUD hideHUDForView:tableView animated:YES];
    [[self parentViewController] dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -
- (void)downloadSelectedDecks {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:tableView animated:YES];
    hud.labelText = @"Downloading...";
    [[flServerCommunicator sharedCommunicator] fetchDecks:[selectedStubs valueForKey:@"deckId"]];
}

@end
