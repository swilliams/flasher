//
//  flAppDelegate.h
//  Flasher
//
//  Created by Scott Williams on 10/17/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <UIKit/UIKit.h>

@class flViewController;

@interface flAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) flViewController *viewController;

@end
