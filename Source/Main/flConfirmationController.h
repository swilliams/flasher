//
//  flConfirmationController.h
//  Flasher
//
//  Created by Scott Williams on 10/31/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "flButton.h"

@protocol flViewControllerDelegate <NSObject>

- (void)viewControllerFinishedOperations:(UIViewController *)vc;

@end

@interface flConfirmationController : UIViewController {
    
    __weak IBOutlet flButton *confirmationButton;
}
- (IBAction)confirmTapped:(id)sender;

@property BOOL didConfirm;
@property (nonatomic, weak) id <flViewControllerDelegate> delegate;

@end
