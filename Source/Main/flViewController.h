//
//  flViewController.h
//  Flasher
//
//  Created by Scott Williams on 10/17/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FLBaseViewController.h"

@class FLToolbar;

@interface flViewController : FLBaseViewController <UICollectionViewDataSource, UICollectionViewDelegate> {
    NSIndexPath *selectedCellIndex;
    BOOL inEditMode;
    
    __weak IBOutlet FLToolbar *toolbar;
    __weak IBOutlet UICollectionView *collectionView;
    IBOutlet UIBarButtonItem *editButton;
    IBOutlet UIBarButtonItem *addButton;
    IBOutlet UIBarButtonItem *downloadButton;
    IBOutlet UIBarButtonItem *trashButton;
    IBOutlet UIBarButtonItem *doneButton;
}

- (IBAction)addDeckTapped:(id)sender;
- (IBAction)downloadTapped:(id)sender;
- (IBAction)editTapped:(id)sender;
- (IBAction)doneTapped:(id)sender;
- (IBAction)trashTapped:(id)sender;

@end
