//
//  flEditQuestionController.m
//  Flasher
//
//  Created by Scott Williams on 10/31/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "flEditQuestionController.h"
#import "flQuestionAnswer.h"

@implementation flEditQuestionController

@synthesize question, delegate;

- (id)initWithQuestion:(flQuestionAnswer *)q {
    self = [super init];
    if (self) {
        self.question = q;
        
        UINavigationItem *n = [self navigationItem];
        [n setTitle:@"Edit Question"];
        
        UIBarButtonItem *bbi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneTapped:)];
        [[self navigationItem] setRightBarButtonItem:bbi];
        
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelTapped:)];
        [[self navigationItem] setLeftBarButtonItem:cancelButton];
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self styleTextView:questionText];
    [self styleTextView:answerText];
}

- (void)viewWillDisappear:(BOOL)animated {
    if (question) {
        question.answerText = answerText.text;
        question.questionText = questionText.text;
    } else {
        NSLog(@"No question was set.");
    }
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    questionText.text = question.questionText;
    answerText.text = question.answerText;
    [questionText becomeFirstResponder];
}

- (void)styleTextView:(UITextView *)tv {
    tv.layer.borderWidth = 1.0f;
    tv.layer.borderColor = [[UIColor colorWithRed:0.59 green:0.59 blue:0.59 alpha:1.0] CGColor];
    tv.layer.cornerRadius = 8;
}

- (IBAction)doneTapped:(id)sender {
    self.question.questionText = questionText.text;
    self.question.answerText = answerText.text;
    if ([self.delegate respondsToSelector:@selector(questionController:didFinishEditingQuestion:)]) {
        [self.delegate questionController:self didFinishEditingQuestion:self.question];
    }
    [[self parentViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelTapped:(id)sender {
    [[self parentViewController] dismissViewControllerAnimated:YES completion:nil];
}

@end
