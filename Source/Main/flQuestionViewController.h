//
//  flQuestionViewController.h
//  Flasher
//
//  Created by Scott Williams on 10/17/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "flEditQuestionController.h"
#import "flConfirmationController.h"
#import "FLBaseViewController.h"

//@class flCard;
@class flCardDeck;
@class flChangeName;
@class RandomTextureLoader;
@class FLCover;
@class FLResults;
@class FLToolbar;
@class FLToolbarLabel;
@class FLPaperView;

@interface flQuestionViewController : FLBaseViewController <QuestionControllerDelegate, UIPopoverControllerDelegate, flViewControllerDelegate> {
    __weak IBOutlet FLToolbarLabel *navbarTitle;
    __weak IBOutlet UIButton *nameButton;
    __weak IBOutlet UILabel *cardCountLabel;
    __weak IBOutlet FLPaperView *paperView;
    FLCover *cover;
    IBOutlet FLToolbar *toolbar;
    
    IBOutlet UIBarButtonItem *addToolbarButton;
    IBOutlet UIBarButtonItem *editToolbarButton;
    IBOutlet UIBarButtonItem *uploadButton;
    IBOutlet UIBarButtonItem *toolbarSpacer;
    IBOutlet UIBarButtonItem *deleteButton;
    IBOutlet UIBarButtonItem *doneToolbarButton;
    IBOutlet UIBarButtonItem *nextToolbarButton;
    
    UIPopoverController *deleteConfirmationPopover;
    UIPopoverController *changeNamePopover;
    
    FLResults *resultsView;
    flQuestionAnswer *currentQuestion;
    flConfirmationController *confirmationController;
    flChangeName *changeNameController;
    RandomTextureLoader *textureLoader;
    BOOL coverIsOn;
}

@property (nonatomic, retain) flCardDeck *deck;

- (IBAction)doneTapped:(id)sender;
- (IBAction)nextTapped:(id)sender;
- (IBAction)addTapped:(id)sender;
- (IBAction)editTapped:(id)sender;
- (IBAction)deleteTapped:(id)sender;
- (IBAction)deckNameTapped:(id)sender;
- (IBAction)uploadTapped:(id)sender;

@end
