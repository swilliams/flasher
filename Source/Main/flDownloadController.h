//
//  flDownloadController.h
//  Flasher
//
//  Created by Scott Williams on 11/3/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface flDownloadController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    
    __weak IBOutlet UITableView *tableView;
    
    NSArray *deckStubs;
    NSMutableArray *selectedStubs;
    NSDateFormatter *dateFormatter;
}

@end
