//
//  flViewController.m
//  Flasher
//
//  Created by Scott Williams on 10/17/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import "flViewController.h"
#import "flDeckStore.h"
#import "flCardDeck.h"
#import "flQuestionViewController.h"
#import "FLCardCell.h"
#import "FLToolbar.h"
#import "flDownloadController.h"
#import "flConstants.h"

NSString *kCellID = @"CardDeckCell";


@implementation flViewController

#pragma mark - Setup
- (void)viewDidLoad
{
    mainView = collectionView;
    [super viewDidLoad];
    [collectionView registerClass:[FLCardCell class] forCellWithReuseIdentifier:kCellID];

    [self registerNotifications];
    [self setupToolbarButtons];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self endEditMode:NO];
}

- (void)registerNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(decksDownloaded:) name:flDecksLoaded object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UI Elements
- (void)setupToolbarButtons {
    UIImage *buttonImage = [[UIImage imageNamed:@"toolbarbutton-back"] resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    
    [[UIBarButtonItem appearance] setBackgroundImage:buttonImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
}

#pragma mark -
#pragma mark Delegate Callbacks

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [[[flDeckStore sharedStore] allDecks] count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    FLCardCell *cell = [cv dequeueReusableCellWithReuseIdentifier:kCellID forIndexPath:indexPath];
    flCardDeck *deck = [[[flDeckStore sharedStore] allDecks] objectAtIndex:indexPath.row];
    cell.textLabel.text = deck.deckName;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    selectedCellIndex = indexPath;
    if (inEditMode) {
        trashButton.enabled = YES;
    } else {
        [self gotoQuestionAt:indexPath];
    }

}

- (void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion {
    [self deselectCurrentCell];
    [collectionView reloadData];
    [super dismissViewControllerAnimated:flag completion:completion];
}

#pragma mark - Events

- (IBAction)addDeckTapped:(id)sender {
    [[flDeckStore sharedStore] createDeckWithName:@"New Deck"];
    [collectionView reloadData];
}

- (IBAction)downloadTapped:(id)sender {
    flDownloadController *downloadController = [[flDownloadController alloc] init];
    UINavigationController *modalController = [[UINavigationController alloc] initWithRootViewController:downloadController];
    modalController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:modalController animated:YES completion:nil];
}

- (IBAction)editTapped:(id)sender {
    [self startEditMode];
    inEditMode = YES;
}

- (IBAction)doneTapped:(id)sender {
    [self endEditMode:YES];
    inEditMode = NO;
}

- (IBAction)trashTapped:(id)sender {
    flCardDeck *selectedDeck = [[[flDeckStore sharedStore] allDecks] objectAtIndex:selectedCellIndex.row];
    [[flDeckStore sharedStore] deleteDeck:selectedDeck];
    [collectionView reloadData];
    trashButton.enabled = NO;
}

#pragma mark -
#pragma mark Actions

- (void)deselectCurrentCell {
    if (selectedCellIndex) {
        [collectionView deselectItemAtIndexPath:selectedCellIndex animated:true];
    }
}

- (void)startEditMode {
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    NSArray *toolbarButtons = [NSArray arrayWithObjects:trashButton, flex, doneButton, nil];
    [toolbar setItems:toolbarButtons animated:YES];
}

- (void)endEditMode:(BOOL)animated {
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    NSArray *toolbarButtons = [NSArray arrayWithObjects:addButton, editButton, flex, downloadButton, nil];
    [toolbar setItems:toolbarButtons animated:animated];
    if (selectedCellIndex) {
        [collectionView deselectItemAtIndexPath:selectedCellIndex animated:YES];
    }
}

- (void)gotoQuestionAt:(NSIndexPath *)indexPath {
    flCardDeck *deck = [[[flDeckStore sharedStore] allDecks] objectAtIndex:indexPath.row];
    flQuestionViewController *questionController = [[flQuestionViewController alloc] initWithNibName:@"flQuestionViewController" bundle:nil];
    questionController.deck = deck;
    [self presentViewController:questionController animated:YES completion:nil];
}



#pragma mark -
#pragma mark NotificationObservers
- (void)decksDownloaded:(NSNotification *)note {
    NSArray *decks = [[note userInfo] objectForKey:@"decks"];
    [[flDeckStore sharedStore] addDecks:decks];
    [collectionView reloadData];
}
@end
