//
//  flQuestionViewController.m
//  Flasher
//
//  Created by Scott Williams on 10/17/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "MBProgressHUD.h"
#import "flConstants.h"
#import "flQuestionViewController.h"
#import "flCardDeck.h"
#import "RandomTextureLoader.h"
#import "flQuestionAnswer.h"
#import "flEditQuestionController.h"
#import "flConfirmationController.h"
#import "flDeckStore.h"
#import "flChangeName.h"
#import "flServerCommunicator.h"
#import "AuthManager.h"
#import "FLCover.h"
#import "FLResults.h"
#import "FLToolbar.h"
#import "FLToolbarLabel.h"
#import "FLAnswerButton.h"
#import "FLPaperView.h"

@interface flQuestionViewController ()

@end

@implementation flQuestionViewController

@synthesize deck;

#pragma mark - Setup

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    mainView = self.view;
    [super viewDidLoad];
    navbarTitle.text = deck.deckName;
    [deck startWithAllQuestions];
    [self setupDeck];
    coverIsOn = YES;
    [self setupToolbar];
    [self setupCover];
    paperView.originalFrame = paperView.frame;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!currentQuestion) {
        [self displayNewQuestion];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupDeck {
    [deck randomizeQuestions];
    currentQuestion = [deck nextQuestion];
    if (currentQuestion) {
        [self displayQuestion];
    }
}

- (void)setupNotificationObservers {
    [super setupNotificationObservers];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deckFinishedUploading:) name:flDeckCreated object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deckFinishedUpdating:) name:flDeckUpdated object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(authCheckSucceeded:) name:flAuthorizedForUpload object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(questionSlidingOff:) name:flQuestionSlidingOff object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(questionSlidOff:) name:flQuestionSlidOff object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startOver:) name:FLStartOver object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startOverWithWrongQuestions:) name:FLStartOverWithWrongQuestions object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(slidePrevented:) name:FLSlidePrevented object:nil];
}

- (void)resetButtons {
    [paperView resetButtons];
    if (currentQuestion.answered) {
        paperView.wrongButton.selected = !currentQuestion.answeredCorrectly;
        paperView.rightButton.selected = currentQuestion.answeredCorrectly;
    }
}

- (void)setupCover {
    if (!cover) {
        float y = paperView.frame.origin.y + paperView.answerLabel.frame.origin.y;
        CGRect frame = CGRectMake(paperView.frame.origin.x + 10, y, paperView.frame.size.width - 20, paperView.answerLabel.frame.size.height);
        cover = [[FLCover alloc] initWithFrame:frame];
        [cover addTarget:self action:@selector(coverTapped:) forControlEvents:UIControlEventTouchUpInside];
        cover.autoresizingMask = paperView.answerLabel.autoresizingMask;
        [self.view addSubview:cover];
    }
    
}

#pragma mark -
#pragma mark Event Handlers

- (IBAction)doneTapped:(id)sender {
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)nextTapped:(id)sender {
    if (!coverIsOn) {
        [self slideCoverOn];
    }
    [self slideInNextQuestion];
}

- (IBAction)addTapped:(id)sender {
    [self displayNewQuestion];
}

- (IBAction)editTapped:(id)sender {
    [self startEditMode];
}

- (IBAction)deleteTapped:(id)sender {
    [self showDeleteConfirmation];
}

- (IBAction)deckNameTapped:(id)sender {
    [self showChangeNamePanel];
}

- (IBAction)uploadTapped:(id)sender {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Uploading...";
    [self createOrUpdateDeck];
}

- (IBAction)coverTapped:(id)sender {
    [self slideCoverOff];
}

#pragma mark - Toolbar Stuff
- (NSArray *)getToolbarButtonsWithoutUpload {
    NSMutableArray *buttons = [toolbar.items mutableCopy];
    [buttons removeObject:uploadButton];
    return buttons;
}

- (NSArray *)getToolbarButtonsWithUpload {
    NSMutableArray *buttons = [toolbar.items mutableCopy];
    [buttons insertObject:uploadButton atIndex:2];
    return buttons;
}

- (void)displayToolbarWithUpload {
    NSArray *buttons = [self getToolbarButtonsWithUpload];
    [toolbar setItems:buttons animated:YES];
}

- (void)displayToolbarWithoutUpload {
    NSArray *buttons = [self getToolbarButtonsWithoutUpload];
    [toolbar setItems:buttons animated:NO];
}

- (void)setupToolbar {
    if ([[AuthManager sharedManager] authorized]) {
        [self displayToolbarWithUpload];
    } else {
        [self displayToolbarWithoutUpload];
    }
}

- (void)displayToolbarButtonsForResults {
    NSArray *buttons = [NSArray arrayWithObjects:toolbarSpacer, doneToolbarButton, nil];
    [toolbar setItems:buttons animated:YES];
}

- (void)displayStandardButtons {
    NSArray *buttons = [NSArray arrayWithObjects:addToolbarButton, editToolbarButton, toolbarSpacer, deleteButton, doneToolbarButton, nextToolbarButton, nil];
    [toolbar setItems:buttons animated:YES];
}

#pragma mark - Sliding Animations
- (FLPaperView *)buildPaperView:(CGRect)withFrame {
    FLPaperView *v = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([FLPaperView class]) owner:nil options:nil] objectAtIndex:0];
    v.frame = withFrame;
    return v;
}

- (FLPaperView *)createNextQuestionView {
    flQuestionAnswer *next = [deck peakNext];
    FLPaperView *nextQuestionView;
    if (next) {
        CGRect frame = paperView.frame;
        frame.origin.x = MAX(self.view.frame.size.width, self.view.frame.size.height) + 50;
        nextQuestionView = [self buildPaperView:frame];
        nextQuestionView.questionLabel.text = next.questionText;
        [self.view addSubview:nextQuestionView];
    }
    return nextQuestionView;
}

- (FLPaperView *)createPrevQuestionView {
    flQuestionAnswer *prev = [deck peakPrev];
    FLPaperView *prevQuestionView;
    if (prev) {
        CGRect frame = paperView.frame;
        frame.origin.x = 0 - frame.size.width;
        prevQuestionView = [self buildPaperView:frame];
        [self.view addSubview:prevQuestionView];
    }
    return prevQuestionView;
}

- (void)slideInNextQuestion {
    [self slideInNextQuestion:YES];
}

- (void)slideInNextQuestion:(BOOL)andCurrentQuestion {
    FLPaperView *nextQuestionView = [self createNextQuestionView];
    [self addResultToDeck];
    if (nextQuestionView) {
        if (andCurrentQuestion) {
            [paperView slideLeft:nil];
        }
        [self.view bringSubviewToFront:cover];
        [nextQuestionView slideIn:^(BOOL finished) {
            [paperView removeFromSuperview];
            paperView = nextQuestionView;
            [self displayNextQuestion];
        }];
    } else {
        [self displayComplete];
    }
}

- (void)slideInPrevQuestion {
    FLPaperView *prevQuestionView = [self createPrevQuestionView];
    if (prevQuestionView) {
        [self addResultToDeck];
        [self.view bringSubviewToFront:cover];
        [prevQuestionView slideIn:^(BOOL finished) {
            [paperView removeFromSuperview];
            paperView = prevQuestionView;
            [self displayPreviousQuestion];
        }];
    }
}


- (void)slideCoverOff {
    coverIsOn = NO;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];

    CGRect frameEnd = cover.frame;
    frameEnd.origin.y = self.view.frame.size.height;

    cover.frame = frameEnd;
    
    [UIView commitAnimations];
}

- (void)slideCoverOn {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDelegate:self];

    CGRect endFrame = cover.frame;
    endFrame.origin.y = paperView.frame.origin.y + paperView.answerLabel.frame.origin.y;
    
    cover.frame = endFrame;
    coverIsOn = YES;

    [UIView commitAnimations];
}

#pragma mark - Actions
- (void)setCountLabel {
    int count, current;
    if ([deck.activeCards count] == 0) {
        count = 0;
        current = 0;
    } else {
        count = [deck.activeCards count];
        current = deck.currentIndex == 0 ? 1 : deck.currentIndex+1;
    }
    NSString *text = [NSString stringWithFormat:@"%d of %d", current, count];
    cardCountLabel.text = text;
}

- (void)changeDeckName:(NSString *)newName {
    deck.deckName = newName;
    navbarTitle.text = newName;
    [[flDeckStore sharedStore] saveChanges];
}

- (void)deleteCurrentQuestion {
    [deck removeQuestion:currentQuestion];
    [[flDeckStore sharedStore] saveChanges];
    [self displayNextQuestion];
}

- (void)displayQuestion {
    paperView.questionLabel.text = currentQuestion.questionText;
    paperView.answerLabel.text = currentQuestion.answerText;
    [self setCountLabel];
    [self resetButtons];
}

- (void)displayNextQuestion {
    currentQuestion = [deck nextQuestion];
    [self displayQuestion];
}

- (void)displayPreviousQuestion {
    currentQuestion = [deck prevQuestion];
    [self displayQuestion];
}

- (void)displayNewQuestion {
    currentQuestion = [[flQuestionAnswer alloc] initWithQuestion:@"" andAnswer:@""];
    [self displayQuestion];
    [self startEditMode];
}

- (void)startEditMode {
    flEditQuestionController *questionController = [[flEditQuestionController alloc] initWithQuestion:currentQuestion];
    questionController.delegate = self;
    UINavigationController *modalController = [[UINavigationController alloc] initWithRootViewController:questionController];
    modalController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:modalController animated:YES completion:nil];
}

- (void)showDeleteConfirmation {
    confirmationController = [[flConfirmationController alloc] init];
    deleteConfirmationPopover = [[UIPopoverController alloc] initWithContentViewController:confirmationController];
    deleteConfirmationPopover.popoverContentSize = CGSizeMake(320, 84);
    confirmationController.delegate = self;
    [deleteConfirmationPopover presentPopoverFromBarButtonItem:deleteButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)showChangeNamePanel {
    changeNameController = [[flChangeName alloc] init];
    changeNameController.textField.text = deck.deckName;
    changeNamePopover = [[UIPopoverController alloc] initWithContentViewController:changeNameController];
    changeNamePopover.popoverContentSize = CGSizeMake(480, 84);
    changeNameController.delegate = self;
    [changeNamePopover presentPopoverFromRect:nameButton.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)createOrUpdateDeck {
    if ([deck isFromServer]) {
        [[flServerCommunicator sharedCommunicator] updateDeck:deck];
    } else {
        [[flServerCommunicator sharedCommunicator] createDeck:deck];
    }
}

- (void)addResultToDeck {
    if (paperView.rightButton.selected) {
        [deck addRightQuestion:currentQuestion];
    } else if (paperView.wrongButton.selected) {
        [deck addWrongQuestion:currentQuestion];
    } else {
        [deck addSkippedQuestion:currentQuestion];
    }
}

#pragma mark - Results

- (void)createResultsView {
    CGRect refFrame = CGRectOffset(self.view.frame, 0, 24);
    CGRect startFrame = CGRectOffset(refFrame, refFrame.size.width, 0);
    if (!resultsView) {
        resultsView = [[FLResults alloc] initWithFrame:startFrame];
    } else {
        resultsView.frame = startFrame;
    }
}

- (void)setResultsViewData {
    int correctCount = deck.numberOfRightAnswers;
    int wrongCount = deck.numberOfWrongAnswers;
    int skippedCount = deck.numberOfSkippedAnswers;
    if (wrongCount + correctCount == 0) {
        resultsView.percentageLabel.text = @"---";
    } else {
        float percent = (float)correctCount / (float)(wrongCount + correctCount) * 100;
        resultsView.percentageLabel.text = [NSString stringWithFormat:@"%.0f%%", percent];
    }
    resultsView.correctLabel.text = [NSString stringWithFormat:@"%d", correctCount];
    resultsView.wrongLabel.text = [NSString stringWithFormat:@"%d", wrongCount];
    resultsView.skippedLabel.text = [NSString stringWithFormat:@"%d", skippedCount];
    resultsView.wrongQuestionsButton.hidden = wrongCount == 0;
}


- (void)displayComplete {
    [self createResultsView];
    [self displayToolbarButtonsForResults];
    
    CGRect endFrame = CGRectOffset(self.view.frame, 0, 24);
    [self.view addSubview:resultsView];
    [self setResultsViewData];
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationCurveLinear
                     animations:^{
                         resultsView.frame = endFrame;
                     }
                     completion:nil];
}

- (void)hideComplete {
    [UIView animateWithDuration:0.5 animations:^{
        resultsView.alpha = 0.0;
    } completion:^(BOOL fin){
        if (fin) [resultsView removeFromSuperview];
        resultsView = nil;
    }];
}

#pragma mark - Callbacks


- (void)startOver:(NSNotification *)n {
    [self setupDeck];
    [self displayStandardButtons];
    [self hideComplete];
    [self.deck startWithAllQuestions];
    paperView.preventSlideLeft = NO;
}

- (void)startOverWithWrongQuestions:(NSNotification *)n {
    [deck startOverWithWrongQuestions];
    [self setupDeck];
    [self displayStandardButtons];
    [self hideComplete];
    paperView.preventSlideLeft = NO;
}

- (void)slidePrevented:(NSNotification *)n {
    if (![deck peakNext]) {
        [self addResultToDeck];
        [self displayComplete];
    }
}

- (void)deckFinishedUploading:(NSNotification *)n {
    deck.serverId = [[n.userInfo objectForKey:@"createdDeckId"] integerValue];
    [[flDeckStore sharedStore] saveChanges];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)deckFinishedUpdating:(NSNotification *)n {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)authCheckSucceeded:(NSNotification *)n {
    [self displayToolbarWithUpload];
}

- (void)questionSlidingOff:(NSNotification *)n {
    if (![deck peakNext]) {
        paperView.preventSlideLeft = YES;
    }
    if (![deck peakPrev]) {
        paperView.preventSlideRight = YES;
    }
    if (!coverIsOn) {
        [self slideCoverOn];
    }
}

- (void)questionSlidOff:(NSNotification *)n {
    NSNumber *directionValue = [[n userInfo] objectForKey:@"direction"];
    VelocityDirection direction = [directionValue intValue];
    if (direction == VELOCITY_LEFT) {
        [self slideInNextQuestion:NO];
    } else if (direction == VELOCITY_RIGHT) {
        [self slideInPrevQuestion];
    }
}

- (void)viewControllerFinishedOperations:(UIViewController *)vc {
    if ([vc isEqual:confirmationController]) {
        [deleteConfirmationPopover dismissPopoverAnimated:YES];
        [self deleteCurrentQuestion];
        confirmationController = nil;
        deleteConfirmationPopover = nil;
    }
    if ([vc isEqual:changeNameController]) {
        [changeNamePopover dismissPopoverAnimated:YES];
        [self changeDeckName:changeNameController.textField.text];
        changeNameController = nil;
        changeNamePopover = nil;
    }
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    [UIView setAnimationDelegate:nil];
}

- (void)questionController:(id)qc didFinishEditingQuestion:(flQuestionAnswer *)q {
    [deck addNewQuestion:q];
    currentQuestion = q;
    [[flDeckStore sharedStore] saveChanges];
    [self displayQuestion];
}


@end
