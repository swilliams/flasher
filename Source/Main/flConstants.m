//
//  flConstants.m
//  Flasher
//
//  Created by Scott Williams on 11/5/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import "flConstants.h"

NSString *const flRemoteDecksReadyToDownload = @"flRemoteDecksReadyToDownload";
NSString *const flRecentDecksLoaded = @"flRecentDecksLoaded";
NSString *const flDecksLoaded = @"flDecksLoaded";
NSString *const flDeckCreated = @"flDeckCreated";
NSString *const flDeckUpdated = @"flDeckUpdated";
NSString *const flAuthorizedForUpload = @"flAuthorizedForUpload";
NSString *const flQuestionSlidingOff = @"flQuestionSlidingOff";
NSString *const flQuestionSlidOff = @"flQuestionSlidOff";
NSString *const FLStartOver = @"FLStartOver";
NSString *const FLStartOverWithWrongQuestions = @"FLStartOverWithWrongQuestions";
NSString *const FLSlidePrevented = @"FLSlidePrevented";
NSString *const FLRemoteError = @"FLRemoteError";