//
//  flConstants.h
//  Flasher
//
//  Created by Scott Williams on 11/5/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>

UIKIT_EXTERN NSString *const flRemoteDecksReadyToDownload;
UIKIT_EXTERN NSString *const flRecentDecksLoaded;
UIKIT_EXTERN NSString *const flDecksLoaded;
UIKIT_EXTERN NSString *const flDeckCreated;
UIKIT_EXTERN NSString *const flDeckUpdated;
UIKIT_EXTERN NSString *const flAuthorizedForUpload;
UIKIT_EXTERN NSString *const flQuestionSlidingOff;
UIKIT_EXTERN NSString *const flQuestionSlidOff;
UIKIT_EXTERN NSString *const FLStartOver;
UIKIT_EXTERN NSString *const FLStartOverWithWrongQuestions;
UIKIT_EXTERN NSString *const FLSlidePrevented;
UIKIT_EXTERN NSString *const FLRemoteError;

typedef enum velocityDirection {
    VELOCITY_LEFT = 1,
    VELOCITY_RIGHT = 2
} VelocityDirection;