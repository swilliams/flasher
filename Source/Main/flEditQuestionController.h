//
//  flEditQuestionController.h
//  Flasher
//
//  Created by Scott Williams on 10/31/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>

@class flQuestionAnswer;

@protocol QuestionControllerDelegate <NSObject>

- (void)questionController:(id)qc didFinishEditingQuestion:(flQuestionAnswer *)q;

@end


@interface flEditQuestionController : UIViewController {
    
    __weak IBOutlet UITextView *questionText;
    __weak IBOutlet UITextView *answerText;
}

@property (strong, nonatomic) flQuestionAnswer *question;
@property (nonatomic, weak) id <QuestionControllerDelegate> delegate;

- (id)initWithQuestion:(flQuestionAnswer *)question;

@end


