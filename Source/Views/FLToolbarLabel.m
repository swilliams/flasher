//
//  FLToolbarLabel.m
//  Flasher
//
//  Created by Scott Williams on 2/5/13.
//  Copyright (c) 2013 Scott Williams. All rights reserved.
//

#import "FLToolbarLabel.h"

@implementation FLToolbarLabel

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.font = [UIFont fontWithName:@"Palatino-Bold" size:20];
        self.textColor = [UIColor whiteColor];
        self.shadowColor = [UIColor darkTextColor];
        self.shadowOffset = CGSizeMake(0, 1);
    }
    return self;
}


@end
