//
//  FLErrorView.m
//  Flasher
//
//  Created by Scott Williams on 2/18/13.
//  Copyright (c) 2013 Scott Williams. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "FLErrorView.h"

@implementation FLErrorView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupStyles];
    }
    return self;
}

- (void)setupStyles {
    
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    self.titleLabel.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.titleLabel.layer.shadowRadius = 0.0;
    self.titleLabel.layer.shadowOpacity = 0.4;
    self.titleLabel.layer.shadowOffset = CGSizeMake(0.0, -1.0);
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    UIEdgeInsets margins = self.titleEdgeInsets;
    self.titleEdgeInsets = UIEdgeInsetsMake(margins.top, 40, margins.bottom, margins.right);
    self.titleLabel.font = [UIFont systemFontOfSize:14];
}


- (void)drawRect:(CGRect)rect {
    //// General Declarations
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //// Color Declarations
    UIColor* gradientDark = [UIColor colorWithRed: 0.456 green: 0.145 blue: 0.145 alpha: 1];
    UIColor* gradientLight = [UIColor colorWithRed: 0.607 green: 0.1 blue: 0.1 alpha: 1];
    UIColor* borderColor = [UIColor colorWithRed: 1 green: 0.571 blue: 0.571 alpha: 1];
    UIColor* gradientShadow = [UIColor colorWithRed: 0.447 green: 0.011 blue: 0.011 alpha: 1];
    
    //// Gradient Declarations
    NSArray* redGradientColors = [NSArray arrayWithObjects:
                                  (id)gradientShadow.CGColor,
                                  (id)[UIColor colorWithRed: 0.527 green: 0.056 blue: 0.056 alpha: 1].CGColor,
                                  (id)gradientLight.CGColor,
                                  (id)gradientDark.CGColor, nil];
    CGFloat redGradientLocations[] = {0, 0.08, 0.13, 1};
    CGGradientRef redGradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)redGradientColors, redGradientLocations);
    
    //// Image Declarations
    UIImage* errorIcon = [UIImage imageNamed: @"error_icon"];
    
    //// Rectangle Drawing
    UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRect: CGRectMake(0, 0, rect.size.width, rect.size.height)];
    CGContextSaveGState(context);
    [rectanglePath addClip];
    CGContextDrawLinearGradient(context, redGradient, CGPointMake(0, 0), CGPointMake(0, rect.size.height), 0);
    CGContextRestoreGState(context);
    
    
    //// Bezier Drawing
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint: CGPointMake(0, rect.size.height)];
    [bezierPath addLineToPoint: CGPointMake(rect.size.width, rect.size.height)];
    [borderColor setStroke];
    bezierPath.lineWidth = 1;
    [bezierPath stroke];
    
    
    //// Icon drawing
    CGContextSaveGState(context);
    [errorIcon drawInRect: CGRectMake(4, 7, errorIcon.size.width, errorIcon.size.height)];
    CGContextRestoreGState(context);
    
    
    //// Cleanup
    CGGradientRelease(redGradient);
    CGColorSpaceRelease(colorSpace);
}

- (void)createMask:(CGRect)maskRect {
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, nil, maskRect);
    maskLayer.path = path;
    CGPathRelease(path);
    self.layer.mask = maskLayer;
}

- (void)slideDown {
    // create the mask, immediately above the bounds of this view
    CGRect maskRect = CGRectMake(0, -self.frame.size.height, self.frame.size.width, self.frame.size.height);
    [self createMask:maskRect];
    
    // perform the animation by sliding down the mask into view
    [CATransaction begin]; {
        [CATransaction setCompletionBlock:^{
            self.layer.mask = nil;
        }];
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position.y"];
        animation.byValue = [NSNumber numberWithFloat:self.frame.size.height];
        [self.layer.mask addAnimation:animation forKey:@"position.y"];
    } [CATransaction commit];
}

- (void)slideUp:(void(^)(void))callback {
    // create the mask, around the view
    CGRect maskRect = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    [self createMask:maskRect];
    [CATransaction begin]; {
        [CATransaction setCompletionBlock:callback];
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position.y"];
        animation.byValue = [NSNumber numberWithFloat:-self.frame.size.height];
        [self.layer.mask addAnimation:animation forKey:@"position.y"];
    } [CATransaction commit];
}


@end
