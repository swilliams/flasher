//
//  FLToolbar.m
//  Flasher
//
//  Created by Scott Williams on 2/4/13.
//  Copyright (c) 2013 Scott Williams. All rights reserved.
//

#import "FLToolbar.h"

@implementation FLToolbar


- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        UIImage *back = [UIImage imageNamed:@"topbar-background"];
        [self setBackgroundImage:back forToolbarPosition:UIToolbarPositionTop barMetrics:UIBarMetricsDefault];
    }
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
