//
//  flCard.m
//  Flasher
//
//  Created by Scott Williams on 12/6/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "flCard.h"

@implementation flCard

@synthesize imageName;

- (void)drawRect:(CGRect)rect {
    int radius = 20;
    UIColor *backgroundColor = imageName ? [UIColor colorWithPatternImage:[UIImage imageNamed:imageName]] : [UIColor darkGrayColor];
    CGRect buttonRect = CGRectMake(radius / 2, radius / 2, rect.size.width - radius, rect.size.height - radius);
    UIBezierPath* roundedRectanglePath = [UIBezierPath bezierPathWithRoundedRect: buttonRect cornerRadius: radius];
    [backgroundColor setFill];
    [roundedRectanglePath fill];
    [[UIColor whiteColor] setStroke];
    [roundedRectanglePath setLineWidth:8];
    [roundedRectanglePath stroke];
}

@end
