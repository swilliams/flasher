//
//  flChangeName.h
//  Flasher
//
//  Created by Scott Williams on 11/1/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "flConfirmationController.h"

@interface flChangeName : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (nonatomic, weak) id <flViewControllerDelegate> delegate;

- (IBAction)okTapped:(id)sender;

@end
