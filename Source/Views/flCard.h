//
//  flCard.h
//  Flasher
//
//  Created by Scott Williams on 12/6/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface flCard : UIButton

@property (nonatomic, strong) NSString *imageName;

@end
