//
//  FLCardCell.h
//  Flasher
//
//  Created by Scott Williams on 2/1/13.
//  Copyright (c) 2013 Scott Williams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLCardCell : UICollectionViewCell {
    UIColor *cardShadowColor;
    UIColor *cardStrokeColor;
    UIColor *textColor;
    UIColor *textShadowColor;
}

@property (nonatomic, strong) UILabel *textLabel;

@end
