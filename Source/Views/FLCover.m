//
//  FLCover.m
//  Flasher
//
//  Created by Scott Williams on 2/17/13.
//  Copyright (c) 2013 Scott Williams. All rights reserved.
//

#import "FLCover.h"
#import "UIColor+HexString.h"

@implementation FLCover

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    //// General Declarations
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //// Color Declarations
    UIColor* cardShadowColor = [UIColor colorWithRed: 0.348 green: 0.348 blue: 0.348 alpha: 0.55];
    UIColor* cardBackground = [UIColor colorWithRed: 0.933 green: 0.933 blue: 0.933 alpha: 1];
    UIColor* outerStrokeColor = [UIColor colorWithHexString:@"#DDDDDD"];
    
    //// Shadow Declarations
    UIColor* cardShadow = cardShadowColor;
    CGSize cardShadowOffset = CGSizeMake(0, 5);
    CGFloat cardShadowBlurRadius = 5;
    
    //// Image Declarations
    UIImage* cardImage = [UIImage imageNamed: @"smalldots"];
    UIColor* cardImagePattern = [UIColor colorWithPatternImage: cardImage];
    
    //// Rounded Rectangle Drawing
    CGRect innerRect = CGRectMake(rect.origin.x + 10, rect.origin.y + 10, rect.size.width - 20, rect.size.height - 20);
    UIBezierPath* roundedRectanglePath = [UIBezierPath bezierPathWithRoundedRect: innerRect cornerRadius: 20];
    CGContextSaveGState(context);
    CGContextSetShadowWithColor(context, cardShadowOffset, cardShadowBlurRadius, cardShadow.CGColor);
    CGContextSaveGState(context);
//    CGContextSetPatternPhase(context, CGSizeMake(0, -50));
    [cardImagePattern setFill];
    [roundedRectanglePath fill];
    CGContextRestoreGState(context);
    CGContextRestoreGState(context);
    
    [cardBackground setStroke];
    roundedRectanglePath.lineWidth = 10;
    [roundedRectanglePath stroke];
    
    [outerStrokeColor setStroke];
    CGRect strokeRect = CGRectMake(rect.origin.x + 5, rect.origin.y + 5, rect.size.width - 10, rect.size.height - 10);
    UIBezierPath* outerStrokeRectPath = [UIBezierPath bezierPathWithRoundedRect:strokeRect cornerRadius:24];
    outerStrokeRectPath.lineWidth = 1;
    [outerStrokeRectPath stroke];
    
}


@end
