//
//  FLResults.m
//  Flasher
//
//  Created by Scott Williams on 1/25/13.
//  Copyright (c) 2013 Scott Williams. All rights reserved.
//

#import "FLResults.h"
#import "flConstants.h"

@implementation FLResults

@synthesize correctLabel, wrongLabel, skippedLabel, percentageLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"FLResults" owner:self options:nil];
        self = [arr objectAtIndex:0];
        self.frame = frame;
    }
    return self;
}

- (IBAction)allQuestionsTapped:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:FLStartOver object:nil];
}

- (IBAction)wrongQuestionsTapped:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:FLStartOverWithWrongQuestions object:nil];
}
@end
