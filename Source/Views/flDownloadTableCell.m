//
//  flDownloadTableCell.m
//  Flasher
//
//  Created by Scott Williams on 11/3/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import "flDownloadTableCell.h"

@implementation flDownloadTableCell

@synthesize decknameLabel, ownernameLabel, createdDateLabel, questionCountLabel, checked;

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        normalImage = [UIImage imageNamed:@"check_unselected"];
        selectedImage = [UIImage imageNamed:@"check_selected"];
    }
    return self;
}


- (void)toggleCheck {
    checked = !checked;
    checkImageView.image = checked ? selectedImage : normalImage;
}

@end
