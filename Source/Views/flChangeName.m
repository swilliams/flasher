//
//  flChangeName.m
//  Flasher
//
//  Created by Scott Williams on 11/1/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import "flChangeName.h"

@implementation flChangeName
@synthesize textField, delegate;

- (IBAction)okTapped:(id)sender {
    if ([self.delegate respondsToSelector:@selector(viewControllerFinishedOperations:)]) {
        [self.delegate viewControllerFinishedOperations:self];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [textField becomeFirstResponder];
}
@end
