//
//  FLResults.h
//  Flasher
//
//  Created by Scott Williams on 1/25/13.
//  Copyright (c) 2013 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FLResults : UIView {
    
    __weak IBOutlet UIImageView *imageView;
}

@property (weak, nonatomic) IBOutlet UILabel *correctLabel;
@property (weak, nonatomic) IBOutlet UILabel *wrongLabel;
@property (weak, nonatomic) IBOutlet UILabel *skippedLabel;
@property (weak, nonatomic) IBOutlet UILabel *percentageLabel;
@property (weak, nonatomic) IBOutlet UIButton *wrongQuestionsButton;

- (IBAction)allQuestionsTapped:(id)sender;
- (IBAction)wrongQuestionsTapped:(id)sender;


@end
