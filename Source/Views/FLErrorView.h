//
//  FLErrorView.h
//  Flasher
//
//  Created by Scott Williams on 2/18/13.
//  Copyright (c) 2013 Scott Williams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLErrorView : UIButton

- (void)slideDown;
- (void)slideUp:(void(^)(void))callback;

@end
