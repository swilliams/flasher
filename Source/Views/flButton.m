//
//  flButton.m
//  Flasher
//
//  Created by Scott Williams on 12/1/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "flButton.h"

@implementation flButton

@synthesize imageName, selectedImageName;

- (void)drawRect:(CGRect)rect
{
    if (!imageName) {
        imageName = @"orange";
    }
    UIImage *buttonImage = [[UIImage imageNamed:imageName] resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    UIImage *selectedImage = [[UIImage imageNamed:selectedImageName] resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    [self setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [self setBackgroundImage:selectedImage forState:UIControlStateSelected];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    self.titleLabel.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.titleLabel.layer.shadowRadius = 0.0;
    self.titleLabel.layer.shadowOpacity = 0.4;
    self.titleLabel.layer.shadowOffset = CGSizeMake(0.0, -1.0);
}


@end
