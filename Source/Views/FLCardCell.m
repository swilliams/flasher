//
//  FLCardCell.m
//  Flasher
//
//  Created by Scott Williams on 2/1/13.
//  Copyright (c) 2013 Scott Williams. All rights reserved.
//

#import "FLCardCell.h"

@implementation FLCardCell

@synthesize textLabel;

const int CARD_MARGIN = 10;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        cardShadowColor = [UIColor colorWithRed: 0.423 green: 0.423 blue: 0.423 alpha: 0.55];
        cardStrokeColor = [UIColor colorWithRed: 0.889 green: 0.886 blue: 0.873 alpha: 1];
        textColor =       [UIColor colorWithRed:0.45f green:0.44f blue:0.42f alpha:1.00f];
        textShadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.75];
        self.backgroundColor = [UIColor clearColor];
        [self createLabel];
    }
    return self;
}

- (int)calculateJitter:(int)ceiling {
    return arc4random_uniform(ceiling * 2) - ceiling;
}

- (CGRect)topCardFromRect:(CGRect)rect {
    return [self topCardFromRect:rect withJitter:0];
}

- (CGRect)topCardFromRect:(CGRect)rect withJitter:(int)jitter {
    int xJitter = [self calculateJitter:jitter];
    int yJitter = [self calculateJitter:jitter];
    return CGRectMake(rect.origin.x + CARD_MARGIN + xJitter,
                      rect.origin.y + CARD_MARGIN + yJitter,
                      rect.size.width - 2 * CARD_MARGIN,
                      rect.size.height - 2 * CARD_MARGIN);
}

- (void)createLabel {
    CGRect rect = CGRectMake(5, 0, self.frame.size.width - 10, self.frame.size.height);
    textLabel = [[UILabel alloc] initWithFrame:[self topCardFromRect:rect]];
    textLabel.backgroundColor = self.backgroundColor;
    textLabel.textAlignment = NSTextAlignmentCenter;
    textLabel.font = [UIFont fontWithName:@"Palatino-Bold" size:36];
    textLabel.numberOfLines = 0;
    textLabel.textColor = textColor;
    textLabel.shadowColor = textShadowColor;
    [self addSubview:textLabel];
}

#pragma mark - Drawing

- (float) radians:(double)degrees
{
    return degrees * M_PI / 180;
};

- (void)drawCardInRect:(CGRect)rect withRotationalJitter:(int)rotationJitter withPositionJitter:(int)positionJitter {
    //// General Declarations
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //// Shadow Declarations
    UIColor* cardShadow = cardShadowColor;
    CGSize cardShadowOffset = CGSizeMake(0.1, 2.1);
    CGFloat cardShadowBlurRadius = 3;
    
    CGRect cardRect = [self topCardFromRect:rect withJitter:positionJitter];
    
    CGFloat midX = cardRect.size.width / 2;
    CGFloat midY = cardRect.size.height / 2;

    CGContextSaveGState(context);

    /// Rotation
    CGContextTranslateCTM(context, midX, midY);
    float degrees = (float)[self calculateJitter:rotationJitter] / 10;
    CGContextRotateCTM(context, [self radians:degrees]);
    CGContextTranslateCTM(context, -midX, -midY);
    
    //// Rounded Rectangle Drawing
    UIBezierPath* roundedRectanglePath = [UIBezierPath bezierPathWithRoundedRect:cardRect cornerRadius: 4];

    CGContextSetShadowWithColor(context, cardShadowOffset, cardShadowBlurRadius, cardShadow.CGColor);
    [[UIColor colorWithPatternImage:[UIImage imageNamed:@"exclusive_paper"]] setFill];
    [roundedRectanglePath fill];

    CGContextRestoreGState(context);
    
    
    /// Stroke
    CGContextSaveGState(context);
    CGContextTranslateCTM(context, midX, midY);
    CGContextRotateCTM(context, [self radians:degrees]);
    CGContextTranslateCTM(context, -midX, -midY);
    [cardStrokeColor setStroke];
    roundedRectanglePath.lineWidth = 1;
    [roundedRectanglePath stroke];
    CGContextRestoreGState(context);
}

- (void)drawRect:(CGRect)rect
{
    [self drawCardInRect:rect withRotationalJitter:30 withPositionJitter:4];
    [self drawCardInRect:rect withRotationalJitter:30 withPositionJitter:4];
    [self drawCardInRect:rect withRotationalJitter:0 withPositionJitter:0];
}


@end
