//
//  FLBaseViewController.m
//  Flasher
//
//  Created by Scott Williams on 2/5/13.
//  Copyright (c) 2013 Scott Williams. All rights reserved.
//

#import "FLBaseViewController.h"
#import "flConstants.h"
#import "FLErrorView.h"

@interface FLBaseViewController ()

@end

@implementation FLBaseViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self checkOrientation:self.interfaceOrientation];
    [self setupNotificationObservers];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Orientation

- (void)useLandscapeBackground {
    mainView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"concrete_landscape"]];
}

- (void)usePortraitBackground {
    mainView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"concrete_portrait"]];
}

- (void)checkOrientation:(UIInterfaceOrientation)orientation {
    if (UIInterfaceOrientationIsPortrait(orientation)) {
        [self usePortraitBackground];
    } else {
        [self useLandscapeBackground];
    }
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self checkOrientation:toInterfaceOrientation];
}

#pragma mark - SharedEvents
- (void)setupNotificationObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(serverError:) name:FLRemoteError object:nil];
}

- (void)serverError:(NSNotification *)n {
    NSString *errorMessage = [n userInfo][@"errorString"];
    [self displayServerError:errorMessage];
}

- (void)displayServerError:(NSString *)error {
    if (!errorView) {
        
        CGRect rect = CGRectMake(0, 44, self.view.frame.size.width, 40);
        errorView = [[FLErrorView alloc] initWithFrame:rect];
        [errorView addTarget:self action:@selector(errorTapped:) forControlEvents:UIControlEventTouchUpInside];
        errorView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self.view addSubview:errorView];

    }
    [errorView setTitle:error forState:UIControlStateNormal];
    [errorView slideDown];
}

- (void)errorTapped:(id)sender {
    if (errorView) {
        [errorView slideUp:^{
            [errorView removeFromSuperview];
            errorView = nil;
        }];
    }
}
@end
