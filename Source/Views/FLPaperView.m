//
//  FLPaperView.m
//  Flasher
//
//  Created by Scott Williams on 2/6/13.
//  Copyright (c) 2013 Scott Williams. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "FLAnswerButton.h"
#import "FLPaperView.h"
#import "UIColor+HexString.h"
#import "flConstants.h"

@implementation FLPaperView

@synthesize preventSlideLeft, preventSlideRight, originalFrame, wrongButton, rightButton;

static float slideThreshold    = 200;
static float velocityThreshold = 300;
static float leftSnapback      = -100;
static float rightSnapback     = 100;

#pragma mark - Setup
- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setupBackground];
        [self attachGestureRecognizer];
    }
    return self;
}


- (id)awakeAfterUsingCoder:(NSCoder *)aDecoder {
    BOOL isJustAPlaceholder = ([[self subviews] count] == 0);
    if (isJustAPlaceholder) {
        FLPaperView* theRealThing = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([FLPaperView class]) owner:nil options:nil] objectAtIndex:0];

        theRealThing.frame = self.frame;    // ... (pass through selected properties)
        theRealThing.autoresizingMask = self.autoresizingMask;

        // convince ARC that we're legit
        CFRelease((__bridge const void*)self);
        CFRetain((__bridge const void*)theRealThing);
        
        return theRealThing;
    }
    return self;
}

- (void)setupBackground {
    UIImage *bg = [UIImage imageNamed:@"exclusive_paper"];
    self.backgroundColor = [UIColor colorWithPatternImage:bg];
}

# pragma mark - Actions

- (IBAction)rightTapped:(id)sender {
    rightButton.selected = !rightButton.selected;
    wrongButton.selected = NO;
}

- (IBAction)wrongTapped:(id)sender {
    wrongButton.selected = !wrongButton.selected;
    rightButton.selected = NO;
}

- (void)resetButtons {
    wrongButton.selected = rightButton.selected = NO;
}

# pragma mark - Gestures
- (void)attachGestureRecognizer {
    self.userInteractionEnabled = YES;
    if (!dragger) {
        dragger = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
        [self addGestureRecognizer:dragger];
    }
}

- (void)handleGesture:(UIPanGestureRecognizer *)sender {
    if (!announcedGesture) {
        [self announceGesture];
    }
    
    CGPoint translation = [sender translationInView:self];
    CGPoint velocity = [sender velocityInView:self];
        
    if ((self.preventSlideLeft && translation.x < leftSnapback) ||
        (preventSlideRight && translation.x > rightSnapback)) {
        [self slideBack:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:FLSlidePrevented object:self userInfo:nil];
        return;
    }
    
    
    CGRect newFrame = originalFrame;
    newFrame.origin.x += translation.x;
    self.frame = newFrame;
    
    if (sender.state == UIGestureRecognizerStateEnded) {
        announcedGesture = NO;
        if (fabsf(translation.x) > slideThreshold || fabsf(velocity.x) > velocityThreshold) {
            [self slide:[self getEndPosition:translation.x] completion:nil];
            [self slidOff:translation];
        } else {
            [self slideBack:nil];
        }
    }
}

- (void)announceGesture {
    announcedGesture = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:flQuestionSlidingOff object:self userInfo:nil];
}

- (void)slidOff:(CGPoint)direction {
    VelocityDirection velocityDirection = direction.x > 0 ? VELOCITY_RIGHT : VELOCITY_LEFT;
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:velocityDirection] forKey:@"direction"];
    [[NSNotificationCenter defaultCenter] postNotificationName:flQuestionSlidOff object:self userInfo:dict];
}

#pragma mark - Animations
- (void)slideBack:(void (^)(BOOL finished))completion {
    [self slide:originalFrame.origin.x duration:0.25f completion:completion];
}

- (void)slideRight:(void (^)(BOOL finished))completion {
    if (!preventSlideRight) {
        [self slide:[self getEndPosition:1] completion:completion];
    }
}

- (void)slideLeft:(void (^)(BOOL finished))completion {
    if (!preventSlideLeft) {
        [self slide:[self getEndPosition:-1] completion:completion];
    }
}

- (float)maxScreenWidth {
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGSize screen = [UIScreen mainScreen].bounds.size;
    float maxWidth = screen.width;
    if (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight) {
        maxWidth = screen.height;
    }
    return maxWidth;
}

- (void)slideIn:(void (^)(BOOL finished))completion {
    float parentWidth = [self maxScreenWidth];
    float myWidth = self.frame.size.width;
    float endPosition = (parentWidth / 2) - (myWidth / 2);
    [self slide:endPosition completion:completion];
}

- (void)slide:(float)end completion:(void (^)(BOOL finished))completion {
    [self slide:end duration:0.3f completion:completion];
}

- (void)slide:(float)end duration:(float)duration completion:(void (^)(BOOL finished))completion {
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationCurveLinear
                     animations:^{
                         CGRect frameEnd = self.frame;
                         frameEnd.origin.x = end;
                         self.frame = frameEnd;
                         originalFrame = frameEnd;
                     }
                     completion:completion];
}

- (float)getEndPosition:(float)direction {
    if (direction < 0) {
        CGSize screen = [UIScreen mainScreen].bounds.size;
        float maxWidth = MAX(screen.width, screen.height) * 2;
        return 0 - maxWidth;
    } else {
        return self.superview.frame.size.width;
    }
}

@end
