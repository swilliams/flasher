//
//  FLPaperView.h
//  Flasher
//
//  Created by Scott Williams on 2/6/13.
//  Copyright (c) 2013 Scott Williams. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FLAnswerButton;

@interface FLPaperView : UIView {

    __weak IBOutlet FLAnswerButton *wrongButton;
    __weak IBOutlet FLAnswerButton *rightButton;
    
    UIPanGestureRecognizer *dragger;
    BOOL announcedGesture;
}

- (void)resetButtons;

@property CGRect originalFrame;
@property BOOL preventSlideRight;
@property BOOL preventSlideLeft;

@property (weak, nonatomic) FLAnswerButton *wrongButton;
@property (weak, nonatomic) FLAnswerButton *rightButton;

@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UILabel *answerLabel;

- (IBAction)rightTapped:(id)sender;
- (IBAction)wrongTapped:(id)sender;

- (void)slideBack:(void (^)(BOOL finished))completion;
- (void)slideRight:(void (^)(BOOL finished))completion;
- (void)slideLeft:(void (^)(BOOL finished))completion;
- (void)slideIn:(void (^)(BOOL finished))completion;

@end
