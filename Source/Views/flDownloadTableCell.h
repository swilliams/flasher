//
//  flDownloadTableCell.h
//  Flasher
//
//  Created by Scott Williams on 11/3/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface flDownloadTableCell : UITableViewCell {
    
    __weak IBOutlet UILabel *decknameLabel;
    __weak IBOutlet UILabel *ownernameLabel;
    __weak IBOutlet UILabel *createdDateLabel;
    __weak IBOutlet UILabel *questionCountLabel;
    __weak IBOutlet UIImageView *checkImageView;
    
    BOOL checked;
    UIImage *normalImage;
    UIImage *selectedImage;
}

@property (weak, nonatomic) UILabel *decknameLabel;
@property (weak, nonatomic) UILabel *ownernameLabel;
@property (weak, nonatomic) UILabel *createdDateLabel;
@property (weak, nonatomic) UILabel *questionCountLabel;
@property BOOL checked;

- (void)toggleCheck;


@end
