//
//  flButton.h
//  Flasher
//
//  Created by Scott Williams on 12/1/12.
//  Copyright (c) 2012 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface flButton : UIButton
@property (nonatomic, strong) NSString *imageName;
@property (nonatomic, strong) NSString *selectedImageName;

@end
