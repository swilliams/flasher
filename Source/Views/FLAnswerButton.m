//
//  FLAnswerButton.m
//  Flasher
//
//  Created by Scott Williams on 2/10/13.
//  Copyright (c) 2013 Scott Williams. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "FLAnswerButton.h"
#import "UIColor+HexString.h"

@implementation FLAnswerButton

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupFont];
        [self setupColors];
    }
    return self;
}

- (void)setupFont {
    UIFont *f = [UIFont fontWithName:@"AlfaSlabOne-Regular" size:36];
    self.titleLabel.font = f;
}
 

- (void)setupColors {
    self.alpha = 0.4;
}

- (void)drawGradient {
    NSLog(@"draw");
    UIColor *baseColor = self.titleLabel.textColor;
    UIColor *darkerColor = [baseColor darkerColor:0.2];
    NSLog(@"base: %@ dark: %@", baseColor, darkerColor);
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.frame;
    gradient.colors = [NSArray arrayWithObjects:(id)[baseColor CGColor], (id)[darkerColor CGColor], nil];
    [self.titleLabel.layer insertSublayer:gradient atIndex:0];
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = selected ? 1 : 0.4;
    }];
    
}


@end
