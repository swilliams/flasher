//
//  FLBaseViewController.h
//  Flasher
//
//  Created by Scott Williams on 2/5/13.
//  Copyright (c) 2013 Scott Williams. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FLErrorView;

@interface FLBaseViewController : UIViewController {
    UIView *mainView;
    FLErrorView *errorView;
}

- (void)displayServerError:(NSString *)error;
- (void)setupNotificationObservers;

@end
